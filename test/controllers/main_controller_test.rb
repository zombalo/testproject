require 'test_helper'

class MainControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do 
    get root_url
    assert_response :success
  end

  test 'should set shop_id in session' do
    post main_locate_url, params: { location: { lat: 55.738456, long: 37.432530 } }
    assert Shop.exists?(session[:current_shop])
    assert_response :success
  end

  test 'should throw error for missed location attr' do
    post main_locate_url, params: { location: { lat: 55.738456 } }
    assert_response 400
  end

  test 'setting shop manually' do
    post main_set_shop_url, params: { shop_picker: { shop: Shop.first.id } }
    assert_equal Shop.first.id, session[:current_shop].to_i
    assert_equal session[:shop_allocated], 'manual'
    assert_response :redirect
    assert_redirected_to root_path
  end
end
