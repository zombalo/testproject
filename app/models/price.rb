class Price < ApplicationRecord
  belongs_to :shop
  belongs_to :product

  validates :shop, uniqueness: { scope: :product }
end
