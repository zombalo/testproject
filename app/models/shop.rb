class Shop < ApplicationRecord
  has_many :prices

  def self.get_closest_shop lat, long
    @shops = Array.new

    Shop.all.each { |shop| @shops << { shop_id: shop.id, distance: distance_to_shop([lat, long], [shop.lat, shop.long]) } }

    @shops.min_by { |shop| shop[:distance] }[:shop_id]
  end

  private
  def self.distance_to_shop loc1, loc2
    loc1[0] = loc1[0].to_f
    loc1[1] = loc1[1].to_f
    loc2[0] = loc2[0].to_f
    loc2[1] = loc2[1].to_f
    rad_per_deg = Math::PI/180
    rkm = 6371
    rm = rkm * 1000

    dlat_rad = (loc2[0]-loc1[0]) * rad_per_deg
    dlon_rad = (loc2[1]-loc1[1]) * rad_per_deg

    lat1_rad, lon1_rad = loc1.map {|i| i * rad_per_deg }
    lat2_rad, lon2_rad = loc2.map {|i| i * rad_per_deg }

    a = Math.sin(dlat_rad/2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad/2)**2
    c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))

    (rm * c).round
  end
end
