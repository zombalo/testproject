class MainController < ApplicationController
  def index
    @shop = session[:current_shop] ? Shop.find(session[:current_shop]) : Shop.last
  end

  def locate
    if params[:location][:lat].nil? || params[:location][:long].nil? || params[:location][:reset] != 'true' && session[:shop_allocated] == 'manual'
      render :status => :bad_request
      return
    end

    session.delete(:shop_allocated)

    @closest_shop = Shop.get_closest_shop params[:location][:lat], params[:location][:long]

    if session[:current_shop].to_i != @closest_shop
      session[:current_shop] = @closest_shop
      render partial: 'show_closest.js.erb'
    end
  end

  def set_shop
    session[:current_shop] = params[:shop_picker][:shop]
    session[:shop_allocated] = 'manual'
    redirect_to root_url
  end
end