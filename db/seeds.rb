# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
Shop.destroy_all
Product.destroy_all

Shop.create(address: 'ул. Воздвиженка, 4/7 стр.1, Москва, 125009', lat: 55.7526875, long: 37.6100625)
Shop.create(address: 'Манежная площадь, 1с2, Москва, 125009', lat: 55.7560449, long: 37.6150251)
Shop.create(address: 'Красная пл., 3, Москва, 109012', lat: 55.7546025, long: 37.6215994)
Shop.create(address: 'Театральный пр-д, 5, Москва, 109012', lat: 55.7601567, long: 37.6246119)
Shop.create(address: 'Мясницкая ул., 24/7 стр.1, Москва, 101000', lat: 55.7623897, long: 37.6361531)

Product.create(name: 'Homo Deus. Краткая история будущего')
Product.create(name: 'Фиолетовая корова. Сделайте свой бизнес выдающимся!')
Product.create(name: 'Доктор Стрэндж. Странные дела')
Product.create(name: 'Полная энциклопедия Marvel')
Product.create(name: 'Как смотреть на картины')
Product.create(name: 'Дети мои')
Product.create(name: 'Как прожить много жизней')
Product.create(name: 'Viva la vagina. Хватит замалчивать скрытые возможности органа, который не принято называть')
Product.create(name: 'Защита от темных искусств')
Product.create(name: 'Искусство тоталитарной Италии')
Product.create(name: 'Омерзительное искусство. Юмор и хоррор шедевров живописи')

Product.all.each do |product|
      Shop.all.each do |shop|
        product.prices.create({ price: rand(450.00...600.00).round(2), shop: shop })
      end
    end
