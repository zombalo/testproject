class ShopProductsPricesSetup < ActiveRecord::Migration[5.2]
  def change
    create_table :shops do |t|
      t.string :address
      t.decimal :lat
      t.decimal :long

      t.timestamps
    end

    create_table :products do |t|
      t.string :name

      t.timestamps
    end

    create_table :prices do |t|
      t.float :price
      t.belongs_to :shop, index: true
      t.belongs_to :product, index: true

      t.timestamps
    end
  end
end
